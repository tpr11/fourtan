package com.personal.fourtan.controllers;

import com.personal.fourtan.model.Post;
import com.personal.fourtan.services.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
@RequestMapping(value = "/post")
public class PostController {

    @Autowired
    private PostService postService;

    @RequestMapping(value = "/{postId}", method = RequestMethod.GET)
    public ResponseEntity<Post> get(@PathVariable(value = "postId") Long id) {
        return new ResponseEntity<>(postService.getPost(id), HttpStatus.OK);
    }

    @RequestMapping(value = "/{postId}/delete", method = RequestMethod.POST)
    public ResponseEntity delete(@PathVariable(value = "postId") Long id,
                                 Principal user) {
        postService.deletePost(user.getName(), id);
        return new ResponseEntity(HttpStatus.OK);
    }
}
