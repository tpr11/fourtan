package com.personal.fourtan.controllers;

import com.personal.fourtan.model.Board;
import com.personal.fourtan.model.BoardWrapper;
import com.personal.fourtan.services.BoardService;
import com.personal.fourtan.services.ThreadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;
import java.util.Map;

@RestController
public class BoardController {

    @Autowired
    private BoardService boardService;

    @Autowired
    private ThreadService threadService;

    @RequestMapping(value = "/boards", method = RequestMethod.GET)
    public ResponseEntity<List<Board>> getAll() {
        return new ResponseEntity<>(boardService.getBoards(), HttpStatus.OK);
    }

    @RequestMapping(value = "/{boardName}", method = RequestMethod.GET)
    public ResponseEntity<BoardWrapper> get(@PathVariable(value = "boardName") String name,
                                            @RequestParam(value = "page", defaultValue = "1") Integer page) {
        page = page > 0 ? page : 1;
        return new ResponseEntity<>(boardService.getBoardFull(name, page), HttpStatus.OK);
    }

    @RequestMapping(value = "/{boardName}/create", method = RequestMethod.POST)
    public ResponseEntity create(@PathVariable(value = "boardName") String name,
                                 @RequestBody Map<String, Object> json,
                                 Principal user) {
        threadService.createThread(
                user.getName(),
                name,
                (String) json.get("title"),
                (String) json.get("content"));
        return new ResponseEntity(HttpStatus.OK);
    }
}
