package com.personal.fourtan.controllers;

import com.personal.fourtan.model.User;
import com.personal.fourtan.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Map;

@RestController
public class AuthenticationController {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public ResponseEntity register(@RequestBody User user) {
        userService.createUser(user);
        return new ResponseEntity(HttpStatus.OK);
    }

}
