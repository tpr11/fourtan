package com.personal.fourtan.controllers;

import com.personal.fourtan.exceptions.UnauthorizedException;
import com.personal.fourtan.model.User;
import com.personal.fourtan.services.UserService;
import jdk.nashorn.internal.ir.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.Map;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public ResponseEntity<User> get(Principal user) {
        return new ResponseEntity<>(userService.getUser(user.getName()), HttpStatus.OK);
    }

    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    public ResponseEntity delete(Principal user) {
        userService.deleteUser(user.getName());
        return new ResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public ResponseEntity update(@RequestBody Map<String, Object> json,
                                 Principal user) {
        User newUser = new User(
                user.getName(),
                (String) json.get("newEmail"),
                (String) json.get("newPassword"));
        userService.updateUser(user.getName(), (String) json.get("oldPassword"), newUser);
        return new ResponseEntity(HttpStatus.OK);
    }

}
