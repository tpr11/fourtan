package com.personal.fourtan.controllers;

import com.personal.fourtan.model.Thread;
import com.personal.fourtan.model.ThreadWrapper;
import com.personal.fourtan.services.PostService;
import com.personal.fourtan.services.ThreadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.Map;

@RestController
@RequestMapping("/thread")
public class ThreadController {

    @Autowired
    private ThreadService threadService;

    @Autowired
    private PostService postService;

    @RequestMapping(value = "/{threadId}", method = RequestMethod.GET)
    public ResponseEntity<ThreadWrapper> get(@PathVariable(value = "threadId") Long id,
                                             @RequestParam(value = "page", defaultValue = "1") Integer page) {
        page = page > 1 ? page : 1;
        return new ResponseEntity<>(threadService.getThreadFull(id, page), HttpStatus.OK);
    }

    @RequestMapping(value = "/{threadId}/delete", method = RequestMethod.POST)
    public ResponseEntity delete(@PathVariable(value = "threadId") Long id,
                                 Principal user) {
        threadService.deleteThread(user.getName(), id);
        return new ResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "/{threadId}/post", method = RequestMethod.POST)
    public ResponseEntity post(@PathVariable(value = "threadId") Long id,
                               @RequestBody Map<String, Object> json,
                               Principal user) {
        postService.createPost(
                user.getName(),
                id,
                (String) json.get("content"));
        return new ResponseEntity(HttpStatus.OK);
    }
}
