package com.personal.fourtan.repositories;

import com.personal.fourtan.model.Post;
import com.personal.fourtan.model.Thread;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PostRepository extends JpaRepository<Post, Long> {

    @Query(value =
            "SELECT * " +
            "FROM posts " +
            "WHERE thread_id = :threadid " +
            "ORDER BY created_at " +
            "OFFSET :offset " +
            "LIMIT :limit ",
            nativeQuery = true)
    List<Post> findByThread(@Param("threadid") Long threadId,
                            @Param("offset") Integer offset,
                            @Param("limit") Integer limit);
    long countByThreadId(Long threadId);
}
