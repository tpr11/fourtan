package com.personal.fourtan.repositories;

import com.personal.fourtan.model.Thread;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ThreadRepository extends JpaRepository<Thread, Long> {

    @Query(value =
            "SELECT thread_id, title, board_id, user_id, created_at, updated_at, " +
            "(SELECT created_at " +
            " FROM posts " +
            " WHERE thread_id = t.thread_id " +
            " ORDER BY created_at DESC " +
            " LIMIT 1) last_post_date " +
            "FROM threads t " +
            "WHERE board_id = :boardid " +
            "ORDER BY last_post_date DESC " +
            "OFFSET :offset " +
            "LIMIT :limit",
            nativeQuery = true)
    List<Thread> findByBoard(@Param("boardid") Long boardId,
                             @Param("offset") Integer offset,
                             @Param("limit") Integer limit);
}
