package com.personal.fourtan.model;

import java.util.List;
import java.util.Objects;

public class BoardWrapper {

    private Board board;
    private List<Thread> threads;

    public BoardWrapper(Board board, List<Thread> threads) {
        this.board = board;
        this.threads = threads;
    }

    public Board getBoard() {
        return board;
    }

    public void setBoard(Board board) {
        this.board = board;
    }

    public List<Thread> getThreads() {
        return threads;
    }

    public void setThreads(List<Thread> threads) {
        this.threads = threads;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BoardWrapper that = (BoardWrapper) o;
        return Objects.equals(board, that.board);
    }

    @Override
    public int hashCode() {
        return Objects.hash(board);
    }
}
