package com.personal.fourtan.model;

import java.util.List;
import java.util.Objects;

public class ThreadWrapper {

    private Thread thread;
    private List<Post> posts;

    public ThreadWrapper(Thread thread, List<Post> posts) {
        this.thread = thread;
        this.posts = posts;
    }

    public Thread getThread() {
        return thread;
    }

    public void setThread(Thread thread) {
        this.thread = thread;
    }

    public List<Post> getPosts() {
        return posts;
    }

    public void setPosts(List<Post> posts) {
        this.posts = posts;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ThreadWrapper that = (ThreadWrapper) o;
        return Objects.equals(thread, that.thread);
    }

    @Override
    public int hashCode() {
        return Objects.hash(thread);
    }
}
