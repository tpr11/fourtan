package com.personal.fourtan.util;

import com.personal.fourtan.model.Board;
import com.personal.fourtan.model.Post;
import com.personal.fourtan.model.Thread;
import com.personal.fourtan.model.User;

public abstract class Validation {

    public static boolean validUser(User user) {
        return  user.getUsername() != null &&
                user.getUsername().matches("^[a-zA-Z0-9_-]{3,32}$") &&
                user.getEmail() != null &&
                user.getEmail().matches("^(([^<>()\\[\\]\\\\.,;:\\s@\"]+(\\.[^<>()\\[\\]\\\\.,;:\\s@\"]+)*)|(\".+\"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$") &&
                user.getPassword() != null &&
                user.getPassword().length() >= 6 &&
                user.getPassword().length() <= 128;
    }

    public static boolean validBoard(Board board) {
        return  board.getName() != null &&
                board.getName().matches("^[a-z0-9-]{1,32}$") &&
                board.getDescription().matches("^.{1,1024}$");
    }

    public static boolean validThread(Thread thread) {
        return  thread.getTitle() != null &&
                thread.getTitle().matches("^.{1,1024}$");
    }

    public static boolean validPost(Post post) {
        return  post.getContent() != null &&
                post.getContent().matches("^.{1,65536}$");
    }

}
