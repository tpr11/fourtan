package com.personal.fourtan.services;


import com.personal.fourtan.model.Thread;
import com.personal.fourtan.model.ThreadWrapper;

import java.util.List;

public interface ThreadService {
    void createThread(String username, String boardName, String title, String content);
    Thread getThread(Long id);
    ThreadWrapper getThreadFull(Long id, Integer page);
    void updateThread(String username, Long threadId, Thread newThread);
    void deleteThread(String username, Long threadId);
}
