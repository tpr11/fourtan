package com.personal.fourtan.services.implementations;

import com.personal.fourtan.exceptions.InvalidDataException;
import com.personal.fourtan.exceptions.NotFoundException;
import com.personal.fourtan.exceptions.UnauthorizedException;
import com.personal.fourtan.model.Board;
import com.personal.fourtan.model.BoardWrapper;
import com.personal.fourtan.model.Thread;
import com.personal.fourtan.repositories.BoardRepository;
import com.personal.fourtan.repositories.ThreadRepository;
import com.personal.fourtan.repositories.UserRepository;
import com.personal.fourtan.services.BoardService;
import com.personal.fourtan.util.Validation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BoardServiceImpl implements BoardService {

    private static final int DEFAULT_PAGE_SIZE = 50;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BoardRepository boardRepository;

    @Autowired
    private ThreadRepository threadRepository;

    @Override
    public List<Board> getBoards() {
        return boardRepository.findAll();
    }

    @Override
    public void createBoard(String username, String name, String description) {
        Board board = new Board(
                name,
                description,
                userRepository.findByUsername(username)
        );
        if (!Validation.validBoard(board)) {
            throw new InvalidDataException("Board data is invalid.");
        }
        boardRepository.save(board);
    }

    @Override
    public Board getBoard(Long id) {
        Optional<Board> boardOptional = boardRepository.findById(id);
        if (!boardOptional.isPresent()) {
            throw new NotFoundException("Board not found.");
        }
        return boardOptional.get();
    }

    @Override
    public BoardWrapper getBoardFull(Long id, Integer page) {
        Board board = getBoard(id);
        List<Thread> threads = threadRepository.findByBoard(id, (page - 1) * DEFAULT_PAGE_SIZE, DEFAULT_PAGE_SIZE);
        return new BoardWrapper(board, threads);
    }

    @Override
    public BoardWrapper getBoardFull(String name, Integer page) {
        Board board = boardRepository.findByName(name);
        List<Thread> threads = threadRepository.findByBoard(board.getId(), (page - 1) * DEFAULT_PAGE_SIZE, DEFAULT_PAGE_SIZE);
        return new BoardWrapper(board, threads);
    }

    @Override
    public void updateBoard(String username, Long boardId, Board newBoard) {
        Board board = getBoard(boardId);
        if (!board.getCreator().getUsername().equals(username)) {
            throw new UnauthorizedException("User can not update board.");
        }
        if (newBoard.getName() != null) {
            board.setName(newBoard.getName());
        }
        if (newBoard.getDescription() != null) {
            board.setDescription(newBoard.getDescription());
        }
        if (!Validation.validBoard(board)) {
            throw new InvalidDataException("Board is invalid.");
        }
        boardRepository.save(board);
    }

    @Override
    public void deleteBoard(String username, Long boardId) {
        Board board = getBoard(boardId);
        if (!board.getCreator().getUsername().equals(username)) {
            throw new UnauthorizedException("User can not delete board.");
        }
        boardRepository.deleteById(boardId);
    }
}
