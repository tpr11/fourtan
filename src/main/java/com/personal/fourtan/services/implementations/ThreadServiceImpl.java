package com.personal.fourtan.services.implementations;

import com.personal.fourtan.exceptions.InvalidDataException;
import com.personal.fourtan.exceptions.NotFoundException;
import com.personal.fourtan.exceptions.UnauthorizedException;
import com.personal.fourtan.model.Post;
import com.personal.fourtan.model.Thread;
import com.personal.fourtan.model.ThreadWrapper;
import com.personal.fourtan.model.User;
import com.personal.fourtan.repositories.BoardRepository;
import com.personal.fourtan.repositories.PostRepository;
import com.personal.fourtan.repositories.ThreadRepository;
import com.personal.fourtan.repositories.UserRepository;
import com.personal.fourtan.services.ThreadService;
import com.personal.fourtan.util.Validation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class ThreadServiceImpl implements ThreadService {

    private static final int DEFAULT_PAGE_SIZE = 10;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BoardRepository boardRepository;

    @Autowired
    private ThreadRepository threadRepository;

    @Autowired
    private PostRepository postRepository;

    @Override
    @Transactional
    public void createThread(String username, String boardName, String title, String content) {
        User user = userRepository.findByUsername(username);
        Thread thread = new Thread(
                title,
                boardRepository.findByName(boardName),
                user
        );
        if (!Validation.validThread(thread)) {
            throw new InvalidDataException("Thread is invalid.");
        }
        threadRepository.save(thread);
        Post post = new Post(
                content,
                thread,
                user
        );
        if (!Validation.validPost(post)) {
            throw new InvalidDataException("Post is invalid.");
        }
        postRepository.save(post);
    }

    @Override
    public Thread getThread(Long id) {
        Optional<Thread> threadOptional = threadRepository.findById(id);
        if (!threadOptional.isPresent()) {
            throw new NotFoundException("Thread not found.");
        }
        return threadOptional.get();
    }

    @Override
    public ThreadWrapper getThreadFull(Long id, Integer page) {
        Thread thread = getThread(id);
        List<Post> posts = postRepository.findByThread(id, (page - 1) * DEFAULT_PAGE_SIZE, DEFAULT_PAGE_SIZE);
        return new ThreadWrapper(thread, posts);
    }

    @Override
    public void updateThread(String username, Long theadId, Thread newThread) {
        Thread thread = getThread(theadId);
        if (!thread.getCreator().getUsername().equals(username)) {
            throw new UnauthorizedException("User can not update thread.");
        }
        if (newThread.getTitle() != null) {
            thread.setTitle(newThread.getTitle());
        }
        if (!Validation.validThread(thread)) {
            throw new InvalidDataException("Thread is invalid.");
        }
        threadRepository.save(thread);
    }

    @Override
    public void deleteThread(String username, Long threadId) {
        Thread thread = getThread(threadId);
        if (!thread.getCreator().getUsername().equals(username)) {
            throw new UnauthorizedException("User can not delete thread.");
        }
        threadRepository.deleteById(threadId);
    }
}
