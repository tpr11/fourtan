package com.personal.fourtan.services.implementations;

import com.personal.fourtan.exceptions.InvalidDataException;
import com.personal.fourtan.exceptions.UnauthorizedException;
import com.personal.fourtan.model.User;
import com.personal.fourtan.repositories.UserRepository;
import com.personal.fourtan.services.UserService;
import com.personal.fourtan.util.Validation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public User getUser(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public void createUser(User user) {
        if (!Validation.validUser(user)) {
            throw new InvalidDataException("User is invalid.");
        }
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        userRepository.save(user);
    }

    @Override
    public void updateUser(String username, String oldPassword, User newUser) {
        User user = getUser(username);
        if (user == null) {
            throw new UsernameNotFoundException(username);
        }

        if (!bCryptPasswordEncoder.matches(oldPassword, user.getPassword())) {
            throw new UnauthorizedException("Current password is incorrect.");
        }

        if (newUser.getEmail() != null) {
            user.setEmail(newUser.getEmail());
        }
        if (newUser.getPassword() != null) {
            user.setPassword(newUser.getPassword());
        }

        if (!Validation.validUser(user)) {
            throw new InvalidDataException("User is invalid.");
        }
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        userRepository.save(user);
    }

    @Override
    public void deleteUser(String username) {
        if (getUser(username) == null) {
            throw new UsernameNotFoundException(username);
        }
        userRepository.deleteByUsername(username);
    }
}
