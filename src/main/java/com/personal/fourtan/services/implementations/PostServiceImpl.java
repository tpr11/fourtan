package com.personal.fourtan.services.implementations;

import com.personal.fourtan.exceptions.InvalidDataException;
import com.personal.fourtan.exceptions.NotFoundException;
import com.personal.fourtan.exceptions.UnauthorizedException;
import com.personal.fourtan.model.Post;
import com.personal.fourtan.repositories.PostRepository;
import com.personal.fourtan.repositories.ThreadRepository;
import com.personal.fourtan.repositories.UserRepository;
import com.personal.fourtan.services.PostService;
import com.personal.fourtan.util.Validation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class PostServiceImpl implements PostService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ThreadRepository threadRepository;

    @Autowired
    private PostRepository postRepository;

    @Override
    public void createPost(String username, Long threadId, String content) {
        Post post = new Post(
                content,
                threadRepository.findById(threadId).get(),
                userRepository.findByUsername(username)
        );
        if (!Validation.validPost(post)) {
            throw new InvalidDataException("Post is invalid.");
        }
        postRepository.save(post);
    }

    @Override
    public Post getPost(Long id) {
        Optional<Post> postOptional = postRepository.findById(id);
        if (!postOptional.isPresent()) {
            throw new NotFoundException("Post not found.");
        }
        return postOptional.get();
    }

    @Override
    public void updatePost(String username, Long postId, Post newPost) {
        Post post = getPost(postId);
        if (!post.getCreator().getUsername().equals(username)) {
            throw new UnauthorizedException("User can not update post.");
        }
        if (newPost.getContent() != null) {
            post.setContent(newPost.getContent());
        }
        if (!Validation.validPost(post)) {
            throw new InvalidDataException("Post is invalid.");
        }
        postRepository.save(post);
    }

    @Override
    @Transactional
    public void deletePost(String username, Long postId) {
        Post post = getPost(postId);
        if (!post.getCreator().getUsername().equals(username)) {
            throw new UnauthorizedException("User can not delete post.");
        }
        if (postRepository.countByThreadId(post.getThread().getId()) == 1) {
            threadRepository.deleteById(post.getThread().getId());
        }
        else {
            postRepository.deleteById(postId);
        }
    }
}
