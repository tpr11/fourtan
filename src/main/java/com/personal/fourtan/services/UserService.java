package com.personal.fourtan.services;

import com.personal.fourtan.exceptions.UnauthorizedException;
import com.personal.fourtan.model.User;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public interface UserService {
    User getUser(String username) throws UsernameNotFoundException;
    void createUser(User user);
    void updateUser(String username, String oldPassword, User newUser) throws UsernameNotFoundException, UnauthorizedException;
    void deleteUser(String username) throws UsernameNotFoundException;
}
