package com.personal.fourtan.services;

import com.personal.fourtan.model.Board;
import com.personal.fourtan.model.BoardWrapper;

import java.util.List;

public interface BoardService {
    List<Board> getBoards();
    void createBoard(String username, String name, String description);
    Board getBoard(Long id);
    BoardWrapper getBoardFull(Long id, Integer page);
    BoardWrapper getBoardFull(String name, Integer page);
    void updateBoard(String username, Long boardId, Board newBoard);
    void deleteBoard(String username, Long boardId);
}
