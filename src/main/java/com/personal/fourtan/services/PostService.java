package com.personal.fourtan.services;


import com.personal.fourtan.model.Post;

public interface PostService {
    void createPost(String username, Long threadId, String content);
    Post getPost(Long id);
    void updatePost(String username, Long postId, Post newPost);
    void deletePost(String username, Long postId);
}
